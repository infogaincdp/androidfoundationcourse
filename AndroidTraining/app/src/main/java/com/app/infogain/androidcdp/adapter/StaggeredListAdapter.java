package com.app.infogain.androidcdp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.infogain.androidcdp.R;
import com.app.infogain.androidcdp.model.Module;

/**
 * Created by sarvaraj on 24/08/16.
 */
public class StaggeredListAdapter extends RecyclerView.Adapter<StaggeredListAdapter.StaggeredView> {

    private Context context;
    private Module module;
    private Typeface heading_font;
    private static ClickListener itemClickListener;

    int[] imgList = {R.drawable.icon1, R.drawable.icon2, R.drawable.icon3, R.drawable.icon4,
            R.drawable.icon5, R.drawable.icon6, R.drawable.icon7, R.drawable.icon8,
            R.drawable.icon9};


    public StaggeredListAdapter(Context context, Module data) {
        this.context = context;
        this.module = data;
    }

    @Override
    public StaggeredView onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_item, parent, false);
        StaggeredView masonryView = new StaggeredView(layoutView);
        return masonryView;
    }

    @Override
    public void onBindViewHolder(StaggeredView holder, int position) {
        holder.imageView.setImageResource(imgList[position]);
        holder.textView.setText(module.getModuleItems().get(position).getStringmodule_title());
        heading_font = Typeface.createFromAsset(context.getAssets(), "fonts/Rafale_bg.otf");
        holder.textView.setTypeface(heading_font);


    }

    @Override
    public int getItemCount() {
        return module.getModuleItems().size();
    }

    class StaggeredView extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imageView;
        TextView textView;

        public StaggeredView(View itemView) {
            super(itemView);
            itemView.setClickable(true);
            itemView.setOnClickListener(this);
            imageView = (ImageView) itemView.findViewById(R.id.img);
            textView = (TextView) itemView.findViewById(R.id.img_name);

        }

        @Override
        public void onClick(View view) {
            itemClickListener.onItemClick(getAdapterPosition(), view);
        }
    }

    public interface ClickListener {
        void onItemClick(int position, View v);

        void onItemLongClick(int position, View v);
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        StaggeredListAdapter.itemClickListener = clickListener;
    }


}
