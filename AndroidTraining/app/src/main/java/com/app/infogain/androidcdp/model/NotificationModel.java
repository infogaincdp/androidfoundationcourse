package com.app.infogain.androidcdp.model;

/**
 * Created by rohit.mehta on 8/30/2016.
 */
public class NotificationModel {

    String heading;
    String subheading;




    public NotificationModel(String heading,String subheading) {
        this.heading = heading;
        this.subheading=subheading;

    }


    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getSubheading() {
        return subheading;
    }

    public void setSubheading(String subheading) {
        this.subheading = subheading;
    }
}
