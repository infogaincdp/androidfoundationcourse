package com.app.infogain.androidcdp.ui.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.app.infogain.androidcdp.R;
import com.app.infogain.androidcdp.util.AppUtils;
import com.app.infogain.androidcdp.util.CheckNetwork;
import com.app.infogain.androidcdp.util.DBSqlite;
import com.app.infogain.androidcdp.util.MyApplication;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.instabug.library.Instabug;

public class ContentListActivity extends MainActivity {
    DBSqlite dbSqlite;
    private static final int TIME_DELAY = 2000;
    ImageView frameLayout;
    Typeface heading_font;
    private static long back_pressed;
    private FirebaseAnalytics mFirebaseAnalytics;
    private Tracker mTracker;
    Toolbar toolbar;
    CollapsingToolbarLayout collapsingToolbar;
    AppBarLayout appBarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_list);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        MyApplication application = (MyApplication) getApplication();
        mTracker = application.getDefaultTracker();

        mTracker.setScreenName("ContentList Screen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        AppUtils.LogAnalyticsEvent(mFirebaseAnalytics, "", "content list Screen", FirebaseAnalytics.Param.CONTENT_TYPE, "content", FirebaseAnalytics.Event.VIEW_ITEM_LIST);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapse_toolbar);
        toolbar.setTitle(" ");
        collapsingToolbar.setTitle(" ");
        heading_font = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Rafale_bg.otf");
        collapsingToolbar.setCollapsedTitleTypeface(heading_font);
        frameLayout = (ImageView) findViewById(R.id.FLShareImage);
        frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.checkConnection(ContentListActivity.this)) {
                    Instabug.getInstance().invoke();
                } else
                    Toast.makeText(ContentListActivity.this, "ENABLE YOUR INTERNET FIRST", Toast.LENGTH_SHORT).show();
            }
        });
        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle("Android Training");
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle("");
                    isShow = false;
                }
            }
        });

        dbSqlite = new DBSqlite(getApplicationContext());


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_notifications) {
            Intent intent = new Intent(ContentListActivity.this, NotificationActivity.class);
            startActivity(intent);

            return true;
        } else {

            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("Action")
                    .setAction("Share")
                    .build());
            AppUtils.LogAnalyticsEvent(mFirebaseAnalytics, "", "share", FirebaseAnalytics.Param.CONTENT_TYPE, "share", FirebaseAnalytics.Event.SHARE);

            Intent i = new Intent(android.content.Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject test");
            i.putExtra(android.content.Intent.EXTRA_TEXT, "Fantastic set of video, tutorials and test for android learning ! download android CDP at here http://mobileapps.infogain.com/CDPTraining.html\n");
            startActivity(Intent.createChooser(i, "Share Via"));


        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        setBack_pressed();
    }

    public void setBack_pressed() {
        if (back_pressed + TIME_DELAY > System.currentTimeMillis()) {
            super.onBackPressed();
        } else {
            Toast.makeText(ContentListActivity.this, "Press once again to exit!",
                    Toast.LENGTH_SHORT).show();
        }
        back_pressed = System.currentTimeMillis();
    }


}

