package com.app.infogain.androidcdp.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by sujeet1.kumar on 8/24/2016.
 */
public class Module {
    @SerializedName("course_content")
    String course_content;

    @SerializedName("modules")
    ArrayList<ModuleItem> modules = new ArrayList<ModuleItem>();

    public ArrayList<ModuleItem> getModuleItems() {
        return modules;
    }

    public void setModuleItems(ArrayList<ModuleItem> modules) {
        this.modules = modules;
    }

    public String getCourse_content() {
        return course_content;
    }

    public void setCourse_content(String course_content) {
        this.course_content = course_content;
    }
}
