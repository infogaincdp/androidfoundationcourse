package com.app.infogain.androidcdp.ui.activities;

import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.infogain.androidcdp.R;
import com.app.infogain.androidcdp.adapter.ContentDetailsPagerAdapter;
import com.app.infogain.androidcdp.model.ModuleContentModel;
import com.app.infogain.androidcdp.model.TabsSubModule;
import com.app.infogain.androidcdp.util.AppUtils;
import com.app.infogain.androidcdp.util.CheckNetwork;
import com.app.infogain.androidcdp.util.MyApplication;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.instabug.library.Instabug;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;

/**
 * Created by sarvaraj on 26/08/16.
 */
public class ContentDetailActivity extends MainActivity {
    private ModuleContentModel module = null;
    public int selected_module_id = 999;
    private TabLayout tabLayout;
    private ArrayList<TabsSubModule> requiredTabs;
    private String moduleId;
    Toolbar toolbar;
    Typeface fontType;
    TextView mTvTitle;
    ImageView instabugreport;
    private FirebaseAnalytics mFirebaseAnalytics;
    private Tracker mTracker;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.xdd) {
            return true;
        } else
            super.onBackPressed();
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_details);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);


        MyApplication application = (MyApplication) getApplication();
        mTracker = application.getDefaultTracker();

        mTracker.setScreenName("ContentDetail Screen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        instabugreport = (ImageView) findViewById(R.id.FLShareImage);
        instabugreport.setOnClickListener(this);
        moduleId = getIntent().getExtras().getString("ModulePos");
        parseData();
        toolbar = (Toolbar) findViewById(R.id.toolbar_content_details);
        toolbar.setTitle("");
        toolbar.setTitleTextColor(getResources().getColor(R.color.color_white)
        );
        setSupportActionBar(toolbar);

        mTvTitle = (TextView) findViewById(R.id.TVToolbarTitle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        checkModule();
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        setTabs();


        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        ContentDetailsPagerAdapter pagerAdapter = new ContentDetailsPagerAdapter(ContentDetailActivity.this.getSupportFragmentManager(), tabLayout.getTabCount(), requiredTabs);

        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                try{
                    viewPager.setCurrentItem(tab.getPosition());
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("Action")
                            .setAction("Content detail item")
                            .setLabel(""+module.getContent().get(selected_module_id).getTabs().get(tab.getPosition()).getTab_title())
                            .build());
                    AppUtils.LogAnalyticsEvent(mFirebaseAnalytics,""+tab.getPosition(),"content details", FirebaseAnalytics.Param.CONTENT_TYPE,module.getContent().get(selected_module_id).getTabs().get(tab.getPosition()).getTab_title(),FirebaseAnalytics.Event.VIEW_ITEM_LIST);
                }catch (Exception e)
                {
                    e.printStackTrace();
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.FLShareImage:
                if (CheckNetwork.checkConnection(ContentDetailActivity.this))
                {
                    Instabug.getInstance().invoke();
                }
                else
                    Toast.makeText(ContentDetailActivity.this,"ENABLE YOUR INTERNET FIRST",Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void parseData() {
        AssetManager assetManager = getAssets();

        try {
            InputStream inputStream = assetManager.open("ModuleContent.json");
            Reader reader = new InputStreamReader(inputStream);
            Gson gson = new Gson();
            module = gson.fromJson(reader, ModuleContentModel.getInstance().getClass());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void checkModule() {
        for (int i = 0; i < module.getContent().size(); i++) {
            if (module.getContent().get(i).getModule_id().equals(moduleId)) {
                fontType = Typeface.createFromAsset(getApplication().getAssets(), "fonts/Rafale_bg.otf");
                selected_module_id = i;
                mTvTitle.setText(module.getContent().get(i).getStringmodule_title());
                mTvTitle.setTypeface(fontType);
                return;
            }
        }
    }

    private void setTabs() {
        requiredTabs = module.getContent().get(selected_module_id).getTabs();
        for (int i = 0; i < module.getContent().get(selected_module_id).getTabs().size(); i++) {
            tabLayout.addTab(tabLayout.newTab().setText(module.getContent().get(selected_module_id).getTabs().get(i).getTab_title()));

        }
    }


}


