package com.app.infogain.androidcdp.ui.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.app.infogain.androidcdp.R;
import com.app.infogain.androidcdp.ui.fragment.DashboardFragment;
import com.app.infogain.androidcdp.util.AppUtils;
import com.app.infogain.androidcdp.util.CheckNetwork;
import com.app.infogain.androidcdp.util.MyApplication;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.instabug.library.Instabug;

public class DashboardActivity extends MainActivity {

    private Toolbar toolbar;
    CollapsingToolbarLayout collapsingToolbarLayout;
    ImageView frameLayout;
    Typeface heading_font;
    private FirebaseAnalytics mFirebaseAnalytics;
    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        MyApplication application = (MyApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("DashBoard Screen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        AppUtils.LogAnalyticsEvent(mFirebaseAnalytics,"","dashboard Screen",FirebaseAnalytics.Param.CONTENT_TYPE,"dashboard",FirebaseAnalytics.Event.VIEW_ITEM_LIST);

        FragmentManager supportFragmentManager = getSupportFragmentManager();
        DashboardFragment dashboardFragment = DashboardFragment.newInstance();
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        frameLayout = (ImageView) findViewById(R.id.FLShareImage);
        frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (CheckNetwork.checkConnection(DashboardActivity.this))
                {
                    Instabug.getInstance().invoke();
                }
                else
                    Toast.makeText(DashboardActivity.this,"ENABLE YOUR INTERNET FIRST",Toast.LENGTH_SHORT).show();

            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        heading_font = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Rafale_bg.otf");




        collapsingToolbarLayout=(CollapsingToolbarLayout)findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(" ");
        collapsingToolbarLayout.setCollapsedTitleTypeface(heading_font);
        collapsingToolbarLayout.setExpandedTitleColor(Color.TRANSPARENT);
        collapsingToolbarLayout.setCollapsedTitleTextColor(Color.WHITE);
        FragmentTransaction ft = supportFragmentManager.beginTransaction();
        ft.replace(R.id.dashboard_fragment, dashboardFragment, "dashboard fragment");
        ft.commit();

        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle("Training Content");
                    isShow = true;
                } else if(isShow) {
                    collapsingToolbarLayout.setTitle(" ");
                    isShow = false;
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_notification, menu);
        // MenuItem refresh = menu.getItem(R.id.action_notificationsa);
        //  refresh.setEnabled(true);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_notifications) {
            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("Action")
                    .setAction("Notification")
                    .build());

            Intent intent = new Intent(DashboardActivity.this, NotificationActivity.class);
            startActivity(intent);
            return true;
        }else
            super.onBackPressed();

    return super.onOptionsItemSelected(item);

    }


}
