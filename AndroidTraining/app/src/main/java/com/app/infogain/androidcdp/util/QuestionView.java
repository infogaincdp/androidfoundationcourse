package com.app.infogain.androidcdp.util;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.infogain.androidcdp.R;
import com.app.infogain.androidcdp.listner.SelectAnswerListner;
import com.app.infogain.androidcdp.model.OptionModel;
import com.app.infogain.androidcdp.model.QustionModel;

import java.util.ArrayList;

/**
 * Created by sujeet1.kumar on 8/26/2016.
 */
public class QuestionView implements View.OnClickListener {
    Context context;
    LinearLayout option1, option2, option3, option4;
    TextView tv_option1, tv_option2, tv_option3, tv_option4, tv_question;
    QustionModel qustionModel;
    SelectAnswerListner selectAnswerListner;
    int position;

    public QuestionView(Context context, View view, SelectAnswerListner selectAnswerListner, int position) {
        this.context = context;
        this.selectAnswerListner = selectAnswerListner;
        this.position = position;
        initView(view);
    }

    private void initView(View view) {
        option1 = (LinearLayout) view.findViewById(R.id.option1);
        option2 = (LinearLayout) view.findViewById(R.id.option2);
        option3 = (LinearLayout) view.findViewById(R.id.option3);
        option4 = (LinearLayout) view.findViewById(R.id.option4);

        tv_option1 = (TextView) view.findViewById(R.id.tv_option1);
        tv_option2 = (TextView) view.findViewById(R.id.tv_option2);
        tv_option3 = (TextView) view.findViewById(R.id.tv_option3);
        tv_option4 = (TextView) view.findViewById(R.id.tv_option4);
        tv_question = (TextView) view.findViewById(R.id.tv_question);

        option1.setOnClickListener(this);
        option2.setOnClickListener(this);
        option3.setOnClickListener(this);
        option4.setOnClickListener(this);
    }

    public void setData(QustionModel qustionModel) {
        this.qustionModel = qustionModel;
        ArrayList<OptionModel> optionModels = qustionModel.getOption();
        tv_question.setText(qustionModel.getQuestion());
        tv_option1.setText(optionModels.get(0).getOption_name());
        tv_option2.setText(optionModels.get(1).getOption_name());
        tv_option3.setText(optionModels.get(2).getOption_name());
        tv_option4.setText(optionModels.get(3).getOption_name());
        setOptionBackGround(qustionModel.getCurrentSelection());
        option1.setBackground(ContextCompat.getDrawable(context, R.drawable.round_ractangle_gray));
        option2.setBackground(ContextCompat.getDrawable(context, R.drawable.round_ractangle_gray));
        option3.setBackground(ContextCompat.getDrawable(context, R.drawable.round_ractangle_gray));
        option4.setBackground(ContextCompat.getDrawable(context, R.drawable.round_ractangle_gray));
    }

    @Override
    public void onClick(View view) {

        option1.setPadding(15, 20, 0, 20);
        option2.setPadding(15, 20, 0, 20);
        option3.setPadding(15, 20, 0, 20);
        option4.setPadding(15, 20, 0, 20);
        switch (view.getId()) {
            case R.id.option1:
                setOptionBackGround("A");
                isCorrectAns("A");
                break;

            case R.id.option2:
                setOptionBackGround("B");
                isCorrectAns("B");
                break;
            case R.id.option3:
                setOptionBackGround("C");
                isCorrectAns("C");
                break;
            case R.id.option4:
                setOptionBackGround("D");
                isCorrectAns("D");
                break;
        }
    }

    private void setOptionBackGround(String id) {
        if (AppUtils.isEmpty(id))
            return;
        switch (id) {
            case "A":
                option1.setBackground(ContextCompat.getDrawable(context, R.drawable.round_ractangle_black));
                option2.setBackground(ContextCompat.getDrawable(context, R.drawable.round_ractangle_gray));
                option3.setBackground(ContextCompat.getDrawable(context, R.drawable.round_ractangle_gray));
                option4.setBackground(ContextCompat.getDrawable(context, R.drawable.round_ractangle_gray));
                break;

            case "B":
                option1.setBackground(ContextCompat.getDrawable(context, R.drawable.round_ractangle_gray));
                option2.setBackground(ContextCompat.getDrawable(context, R.drawable.round_ractangle_black));
                option3.setBackground(ContextCompat.getDrawable(context, R.drawable.round_ractangle_gray));
                option4.setBackground(ContextCompat.getDrawable(context, R.drawable.round_ractangle_gray));
                break;
            case "C":
                option1.setBackground(ContextCompat.getDrawable(context, R.drawable.round_ractangle_gray));
                option2.setBackground(ContextCompat.getDrawable(context, R.drawable.round_ractangle_gray));
                option3.setBackground(ContextCompat.getDrawable(context, R.drawable.round_ractangle_black));
                option4.setBackground(ContextCompat.getDrawable(context, R.drawable.round_ractangle_gray));
                break;
            case "D":
                option1.setBackground(ContextCompat.getDrawable(context, R.drawable.round_ractangle_gray));
                option2.setBackground(ContextCompat.getDrawable(context, R.drawable.round_ractangle_gray));
                option3.setBackground(ContextCompat.getDrawable(context, R.drawable.round_ractangle_gray));
                option4.setBackground(ContextCompat.getDrawable(context, R.drawable.round_ractangle_black));
                break;

        }

    }

    private void isCorrectAns(String selectedValue) {
        qustionModel.setCurrentSelection(selectedValue);
        qustionModel.setAttamptQuestion(true);
        qustionModel.setCorrect(qustionModel.getAns().equalsIgnoreCase(selectedValue));
        if (selectAnswerListner != null)
            selectAnswerListner.answerCallBack(qustionModel, position);
    }

    public QustionModel getSelectAns() {
        return qustionModel;
    }

}
