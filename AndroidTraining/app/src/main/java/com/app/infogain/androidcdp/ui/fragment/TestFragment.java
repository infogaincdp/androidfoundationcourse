/*
 * Copyright (C) 2013 Andreas Stuetz <andreas.stuetz@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.app.infogain.androidcdp.ui.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.app.infogain.androidcdp.R;
import com.app.infogain.androidcdp.listner.SelectAnswerListner;
import com.app.infogain.androidcdp.model.QustionModel;
import com.app.infogain.androidcdp.util.QuestionView;

public class TestFragment extends Fragment implements View.OnClickListener {

	private static final String ARG_POSITION = "position";

	private int position;
	LinearLayout  linearLayout ;
	QuestionView questionView ;
	QustionModel qustionList ;
	SelectAnswerListner selectAnswerListner ;
	public static TestFragment newInstance(QustionModel qustionModel) {
		TestFragment f = new TestFragment();
		Bundle b = new Bundle();
		b.putParcelable(ARG_POSITION, qustionModel);
		f.setArguments(b);

		return f;
	}

	public void addListner(SelectAnswerListner selectAnswerListner, int position){
		this.selectAnswerListner = selectAnswerListner ;
		this.position = position ;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		qustionList = getArguments().getParcelable(ARG_POSITION);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view ;
		view  = inflater.inflate(R.layout.test_screen,container,false);
		initView(view);
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				linearLayout.setVisibility(View.VISIBLE);
			}
		},500);


		return view ;
	}

	private void initView(View view) {
		linearLayout  = (LinearLayout) view.findViewById(R.id.test_praent);
		questionView = new QuestionView(getActivity(),view,selectAnswerListner,position);
        questionView.setData(qustionList);
	}

	@Override
	public void onClick(View view) {

	}

}