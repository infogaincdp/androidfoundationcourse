package com.app.infogain.androidcdp.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.infogain.androidcdp.R;
import com.app.infogain.androidcdp.listner.DashboardClickListner;
import com.app.infogain.androidcdp.model.DataModel;
import com.app.infogain.androidcdp.ui.fragment.ContentListFragment;

import java.util.ArrayList;


public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> implements View.OnClickListener {

    private ArrayList<DataModel> dataSet;
    DashboardClickListner clickListner;
    Context context;
    Typeface heading_font;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView textViewName;
        ImageView imageViewIcon;

        LinearLayout parentaview;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.cardView=(CardView)itemView.findViewById(R.id.card_view);
            this.textViewName = (TextView) itemView.findViewById(R.id.textViewName);
            this.imageViewIcon = (ImageView) itemView.findViewById(R.id.imageView);
            this.parentaview = (LinearLayout) itemView.findViewById(R.id.parentaview);
        }
    }

    public CustomAdapter(ArrayList<DataModel> data, ContentListFragment clickListner,Context context) {
        this.dataSet = data;
        this.clickListner = (DashboardClickListner) clickListner;
        this.context=context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cards_layout, parent, false);

        // view.setOnClickListener(CustomAdapter.this);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        if (listPosition==0)
        {
            holder.imageViewIcon.setBackgroundColor(Color.parseColor("#19B5C4"));








        }
        else {
            holder.imageViewIcon.setBackgroundColor(Color.parseColor("#f99732"));
        }
        TextView textViewName = holder.textViewName;
        ImageView imageView = holder.imageViewIcon;
        textViewName.setText(dataSet.get(listPosition).getName());
        imageView.setImageResource(dataSet.get(listPosition).getImage());
        holder.parentaview.setOnClickListener(CustomAdapter.this);
        holder.parentaview.setTag(listPosition);
        heading_font = Typeface.createFromAsset(context.getAssets(),  "fonts/Rafale_bg.otf");

       textViewName.setTypeface(heading_font);








    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public void onClick(View view) {
        clickListner.itemOnClick(Integer.parseInt(view.getTag().toString()));
    }

}
