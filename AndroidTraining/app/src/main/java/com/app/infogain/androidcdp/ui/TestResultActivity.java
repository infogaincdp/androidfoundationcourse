package com.app.infogain.androidcdp.ui;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.app.infogain.androidcdp.R;
import com.app.infogain.androidcdp.constant.AppConstant;
import com.app.infogain.androidcdp.model.QustionModel;
import com.app.infogain.androidcdp.ui.activities.MainActivity;
import com.app.infogain.androidcdp.ui.fragment.TestResultFragment;
import com.app.infogain.androidcdp.util.AppUtils;
import com.app.infogain.androidcdp.util.MyApplication;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;

public class TestResultActivity extends MainActivity {

    ArrayList<QustionModel> qustionLists = new ArrayList<>();
    private Toolbar toolbar;
    CollapsingToolbarLayout collapsingToolbarLayout;
    private FirebaseAnalytics mFirebaseAnalytics;
    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_result);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        AppUtils.LogAnalyticsEvent(mFirebaseAnalytics,""," Test Result Screen",FirebaseAnalytics.Param.CONTENT_TYPE,"content","screen open");

        MyApplication application = (MyApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("MCQ Result Screen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());


        qustionLists = getIntent().getParcelableArrayListExtra(AppConstant.TEST_RESULT_LIST);

        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);




        collapsingToolbarLayout=(CollapsingToolbarLayout)findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(" ");

        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle("Scorecard");
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbarLayout.setTitle(" ");
                    isShow = false;
                }
            }
        });

        FragmentManager fragmentManager  = getSupportFragmentManager();
        TestResultFragment testResultFragment = TestResultFragment.newInstant();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(AppConstant.TEST_RESULT_LIST,qustionLists);
        testResultFragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container_layout,testResultFragment,"result_fragmnet");
        fragmentTransaction.commit();
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.btn_back));
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onBackPressed();
        return super.onOptionsItemSelected(item);

    }
}
