package com.app.infogain.androidcdp.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by saurabh.khanna on 8/29/2016.
 */
public class ModuleContentModel {

    private static ModuleContentModel instance = null;

    private ModuleContentModel() {
    }

    public static ModuleContentModel getInstance() {
        if (instance == null) {
            instance = new ModuleContentModel();
        }
        return instance;
    }

    @SerializedName("content")
    ArrayList<ModuleContentTabs> content = new ArrayList<ModuleContentTabs>();


    public ArrayList<ModuleContentTabs> getContent() {
        return content;
    }

    public void setContent(ArrayList<ModuleContentTabs> content) {
        this.content = content;
    }
}

