package com.app.infogain.androidcdp.ui.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.app.infogain.androidcdp.R;
import com.app.infogain.androidcdp.constant.AppConstant;
import com.app.infogain.androidcdp.listner.SelectAnswerListner;
import com.app.infogain.androidcdp.model.QuestionPaperModel;
import com.app.infogain.androidcdp.model.QustionModel;
import com.app.infogain.androidcdp.ui.TestResultActivity;
import com.app.infogain.androidcdp.ui.fragment.TestFragment;
import com.app.infogain.androidcdp.util.AppUtils;
import com.app.infogain.androidcdp.util.MyApplication;
import com.app.infogain.androidcdp.util.PagerSlidingTabStrip;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;

public class TestActivity extends MainActivity implements SelectAnswerListner, View.OnClickListener {


    private PagerSlidingTabStrip tabs;
    private ViewPager pager;
    private TestPagerAdapter adapter;
    ArrayList<QustionModel> qustionLists = new ArrayList<>();
    TextView textTime, endTest;
    private CountDownTimer countDownTimer;
    private boolean timerHasStarted = false;
    public long startTime = 60 * 1000;
    public long totalTime = 19;
    private final long interval = 1 * 1000;
    EndTestDialog dialog = null;
    private FirebaseAnalytics mFirebaseAnalytics;
    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        MyApplication application = (MyApplication) getApplication();
        mTracker = application.getDefaultTracker();

        mTracker.setScreenName("MCQ Test screen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        AppUtils.LogAnalyticsEvent(mFirebaseAnalytics,"","test Screen",FirebaseAnalytics.Param.CONTENT_TYPE,"MCQ Test","Test Screen Open");

        initView();
    }


    private void initView() {
        endTest = (TextView) findViewById(R.id.tv_next);
        tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        pager = (ViewPager) findViewById(R.id.pager);
        textTime = (TextView) findViewById(R.id.tv_time);

        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        pager.setPageMargin(pageMargin);
        endTest.setOnClickListener(this);

        setTestTimer();
    }

    private void setTestTimer() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                endTest.setVisibility(View.VISIBLE);
                countDownTimer = new MyCountDownTimer(startTime, interval);
                textTime.setText(textTime.getText() + String.valueOf(startTime / 1000));
                parseData();
                if (!timerHasStarted) {
                    countDownTimer.start();
                    timerHasStarted = true;
                } else {
                    countDownTimer.cancel();
                    timerHasStarted = false;
                }
            }
        }, 1000);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void parseData() {
        AssetManager assetManager = TestActivity.this.getAssets();
        QuestionPaperModel module = null;
        try {
            InputStream ims = assetManager.open("QuestionPaper.json");
            Reader reader = new InputStreamReader(ims);
            Gson gson = new Gson();
            module = gson.fromJson(reader, QuestionPaperModel.class);
            qustionLists.clear();
            qustionLists = module.getData();
            adapter = new TestPagerAdapter(getSupportFragmentManager(), qustionLists, TestActivity.this);
            pager.setAdapter(adapter);
            pager.setCurrentItem(0);
            tabs.setViewPager(pager);
            Log.e("question list size", module.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void answerCallBack(QustionModel qustionModel, int position) {
        qustionLists.remove(position);
        qustionLists.add(position, qustionModel);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
//            case R.id.header_back_layout:
//                countDownTimer.cancel();
//                break;
            case R.id.tv_next:
                showDialog1(1);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        showDialog1(0);
    }

    public void showDialog1(int flag) {
        try {
            if (TestActivity.this == null)
                return;
            // countDownTimer.cancel();
            if (dialog != null) {
                dialog.updateFlag(flag);
                return;
            }
            dialog = new EndTestDialog(TestActivity.this, flag);
            dialog.setCancelable(false);
            dialog.show();
        }catch (Exception e)
        {
            dialog = null ;
            e.printStackTrace();
        }
    }

    public class TestPagerAdapter extends FragmentPagerAdapter {
        ArrayList<QustionModel> qustionLists;
        SelectAnswerListner selectAnswerListner;

        public TestPagerAdapter(FragmentManager fm, ArrayList<QustionModel> qustionLists, SelectAnswerListner selectAnswerListner) {
            super(fm);
            this.qustionLists = qustionLists;
            this.selectAnswerListner = selectAnswerListner;
        }




        @Override
        public CharSequence getPageTitle(int position) {
            return "" + (position + 1);
        }

        @Override
        public int getCount() {
            return qustionLists.size();
        }

        @Override
        public Fragment getItem(int position) {
            TestFragment testFragment = TestFragment.newInstance(qustionLists.get(position));
            testFragment.addListner(selectAnswerListner, position);
            return testFragment;
        }
    }

    public class MyCountDownTimer extends CountDownTimer {
        public MyCountDownTimer(long startTime, long interval) {
            super(startTime, interval);
        }

        @Override
        public void onFinish() {
            if (totalTime > 0) {
                if (totalTime < 10)
                    textTime.setText("0" + totalTime + " : " + "00");
                else
                    textTime.setText(totalTime + " : " + "00");

                countDownTimer = new MyCountDownTimer(startTime, interval);
                countDownTimer.start();
                timerHasStarted = true;
            } else if (totalTime == 0) {
                textTime.setText("00" + " : " + "59");
                countDownTimer = new MyCountDownTimer(startTime, interval);
                countDownTimer.start();
                timerHasStarted = true;
            } else {
                textTime.setText("00:00");
                showDialog1(2);
            }
        }


        @Override
        public void onTick(long millisUntilFinished) {
            long min = millisUntilFinished / 1000;
         //   Log.e("seconds : ", " " + min);
            if (totalTime > -1 && min > -1) {
                if (totalTime < 10) {
                    if (min < 10)
                        textTime.setText("0" + totalTime + " : 0" + min);
                    else
                        textTime.setText("0" + totalTime + " : " + min);
                    if (min == 1)
                        totalTime = totalTime - 1;
                } else {
                    if (min < 10)
                        textTime.setText(totalTime + " : 0" + min);
                    else
                        textTime.setText(totalTime + " : " + min);
                    if (min == 1)
                        totalTime = totalTime - 1;
                }
            } else {
                countDownTimer.cancel();
                textTime.setText("00:00");
            }
        }
    }

    @Override
    protected void onDestroy() {
        if(countDownTimer!=null)
        countDownTimer.cancel();
        if (dialog!=null && dialog.isShowing())
            dialog.dismiss();
        super.onDestroy();
    }


    public class EndTestDialog extends Dialog implements View.OnClickListener {
        TextView titleView;
        Button yesBtn, noBtn;
        Activity activity;
        Typeface fontType;
        int flag;

        public EndTestDialog(Context context, int flag) {
            super(context);
            activity = (TestActivity) context;
            this.flag = flag;
        }

        public void updateFlag(int flag) {
            this.flag = flag;
        }


        @Override
        public void onCreate(Bundle savedInstanceState) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_end_test);
            initViews();
        }

        private void initViews() {

            titleView = (TextView) findViewById(R.id.logout_main);
            fontType = Typeface.createFromAsset(activity.getAssets(),  "fonts/Roboto-Regular.ttf");
            titleView.setTypeface(fontType);
            yesBtn = (Button) findViewById(R.id.yesBtn);
            yesBtn.setTypeface(fontType);
            noBtn = (Button) findViewById(R.id.noBtn);
            noBtn.setTypeface(fontType);
            yesBtn.setOnClickListener(this);
            noBtn.setOnClickListener(this);

            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("Action")
                    .setAction("MCQ Test Screen Dialog")
                    .build());
            AppUtils.LogAnalyticsEvent(mFirebaseAnalytics,"","test Screen dialog",FirebaseAnalytics.Param.CONTENT_TYPE,"MCQ Test Dialog",FirebaseAnalytics.Event.VIEW_ITEM);

        }

        @Override
        public void onClick(View v) {
            dialog = null;
            switch (v.getId()) {
                case R.id.yesBtn:
                    if (flag == 1) {
                        callResultScreen();
                    } else if (flag == 2) {
                        callResultScreen();
                    }
                    finish();
                    break;
                case R.id.noBtn:
                    if (flag == 2) {
                        dismiss();
                        finish();
                    } else
                        dismiss();
                    break;
                default:
                    break;
            }
        }

        private void callResultScreen() {
            dismiss();
            Intent intent = new Intent(getContext(), TestResultActivity.class);
            intent.putExtra(AppConstant.TEST_RESULT_LIST, qustionLists);
            startActivity(intent);

        }


    }
}
