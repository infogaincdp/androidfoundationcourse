package com.app.infogain.androidcdp.util;

import android.app.Application;
import android.graphics.Color;
import android.graphics.drawable.Drawable;

import com.app.infogain.androidcdp.R;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.instabug.library.Instabug;
import com.instabug.library.InstabugFeedbackActivity;

/**
 * Created by rohit.mehta on 8/29/2016.
 */
public class MyApplication extends Application {
   private Tracker mTracker;
    @Override
    public void onCreate() {
        super.onCreate();
       callInstabug();
        getDefaultTracker();

    }


    private void callInstabug() {
       int bgColor = getResources().getColor(R.color.color_white);
        int orangeColor = getResources().getColor(R.color.sky_blue_theme);
        int transparentColor = getResources().getColor(R.color.instabug_transparent_color);
        Drawable headerBg = getResources().getDrawable(R.drawable.instabugback);

        Instabug.initialize(MyApplication.this,getString(R.string.AppToken) )
                .setInvocationEvent(Instabug.IBGInvocationEvent.IBGInvocationEventNone)
                .setAnnotationActivityClass(InstabugFeedbackActivity.class)
                .setPostBugReportMessage("FEEDBACK SUCCESSFULLY SUBMITTED")
                .setShowTutorial(true)
                .setTrackUserSteps(true)
                .enableEmailField(true, true)
                .setInvalidCommentAlertText("FEEDBACK MESSAGE CANNOT BE NULL")
                .setCommentIsRequired(true)
                .setIsTrackingUserSteps(true)
                .setPromptColors(Color.WHITE,
                        getResources().getColor(R.color.sky_blue_theme))
                .setSdkStyle(null, headerBg, bgColor, headerBg, bgColor)
                .setEnableOverflowMenuItem(true).setDebugEnabled(false);

    }


    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }

}
