package com.app.infogain.androidcdp.model;

import java.util.ArrayList;

/**
 * Created by sujeet1.kumar on 8/26/2016.
 */
public class QuestionPaperModel {
    String time ;
    ArrayList<QustionModel> data = new ArrayList<>();

    public ArrayList<QustionModel> getData() {
        return data;
    }

    public void setData(ArrayList<QustionModel> data) {
        this.data = data;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


}
