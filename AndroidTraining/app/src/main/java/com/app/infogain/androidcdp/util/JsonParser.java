package com.app.infogain.androidcdp.util;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * Created by sujeet1.kumar on 8/24/2016.
 */
public class JsonParser {

    Context context ;

    public JsonParser(Context context){
        this.context = context;

    }
    public String loadJSONFromAsset(String fileName) {
        String json = null;
        try {

            InputStream is = context.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public Reader loadJSONFromAsset(String fileName,Context context) {
        Reader reader = null;
        try {

            InputStream ims = context.getAssets().open(fileName);
            int size = ims.available();
            if (size>0)
                reader = new InputStreamReader(ims);
            // byte[] buffer = new byte[size];
            // is.read(buffer);
            ims.close();
            //  json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return reader;
    }

}
