package com.app.infogain.androidcdp.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.app.infogain.androidcdp.R;
import com.app.infogain.androidcdp.ui.activities.TestActivity;


public class EndTestDialog extends Dialog implements View.OnClickListener {
    TextView titleView;
    Button yesBtn, noBtn;
    Activity activity;

    public EndTestDialog(Context context) {
        super(context);
        activity = (TestActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_end_test);
        initViews();

    }

    private void initViews() {

        titleView = (TextView) findViewById(R.id.logout_main);
        yesBtn = (Button) findViewById(R.id.yesBtn);
        noBtn = (Button) findViewById(R.id.noBtn);
        yesBtn.setOnClickListener(this);
        noBtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.yesBtn:
                Toast.makeText(activity, "no_internet_available", Toast.LENGTH_LONG).show();
                break;
            case R.id.noBtn:
                dismiss();
                break;
            default:
                break;
        }
    }
}
