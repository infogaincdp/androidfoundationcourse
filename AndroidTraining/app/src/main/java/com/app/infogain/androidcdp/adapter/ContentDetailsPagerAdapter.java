package com.app.infogain.androidcdp.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.app.infogain.androidcdp.model.TabsSubModule;
import com.app.infogain.androidcdp.ui.fragment.ContentDetailsFragment;

import java.util.ArrayList;

/**
 * Created by sarvaraj on 26/08/16.
 *
 * e
 */
public class ContentDetailsPagerAdapter extends FragmentStatePagerAdapter {


    int mNumOfTabs;
    ArrayList<TabsSubModule> tabsData;




    public ContentDetailsPagerAdapter(FragmentManager fm, int NumOfTabs, ArrayList<TabsSubModule> requiredTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.tabsData = requiredTabs;
    }

    @Override
    public Fragment getItem(int position) {

        //return new ContentDetailsFragment();

        switch (position) {
            case 0:

             Bundle tab_bundle1=   new Bundle();
                tab_bundle1.putParcelable("tabsData", tabsData.get(0));
                tab_bundle1.putBoolean("ItIsVideoTab",false);
                ContentDetailsFragment tab1 = new ContentDetailsFragment();
                tab1.setArguments(tab_bundle1);
                return tab1;
            case 1:
                Bundle tab_bundle2 =  new Bundle();
                tab_bundle2.putParcelable("tabsData", tabsData.get(1));
                tab_bundle2.putBoolean("ItIsVideoTab",true);
                ContentDetailsFragment tab2 = new ContentDetailsFragment();
                tab2.setArguments(tab_bundle2);
                return tab2;
            case 2:
                Bundle tab_bundle3 = new Bundle();
                tab_bundle3.putParcelable("tabsData", tabsData.get(2));
                tab_bundle3.putBoolean("ItIsVideoTab",false);
                ContentDetailsFragment tab3 = new ContentDetailsFragment();
                tab3.setArguments(tab_bundle3);
                return tab3;
        }
        return null;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

}
