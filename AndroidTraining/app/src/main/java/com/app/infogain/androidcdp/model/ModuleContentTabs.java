package com.app.infogain.androidcdp.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by saurabh.khanna on 8/29/2016.
 */
public class ModuleContentTabs {

    @SerializedName("module_id")
    String module_id;
    @SerializedName("module_name")
    String Stringmodule_title;

    @SerializedName("tabs")
    ArrayList<TabsSubModule> tabs = new ArrayList<TabsSubModule>();

    public ArrayList<TabsSubModule> getTabs() {
        return tabs;
    }

    public void setTabs(ArrayList<TabsSubModule> tabs) {
        this.tabs = tabs;
    }

    public String getModule_id() {
        return module_id;
    }

    public void setModule_id(String module_id) {
        this.module_id = module_id;
    }

    public String getStringmodule_title() {
        return Stringmodule_title;
    }

    public void setStringmodule_title(String stringmodule_title) {
        Stringmodule_title = stringmodule_title;
    }


}
