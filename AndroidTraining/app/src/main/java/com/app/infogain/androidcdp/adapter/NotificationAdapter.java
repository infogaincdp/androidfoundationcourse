package com.app.infogain.androidcdp.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.infogain.androidcdp.R;
import com.app.infogain.androidcdp.listner.DashboardClickListner;
import com.app.infogain.androidcdp.model.DataModel;
import com.app.infogain.androidcdp.model.NotificationModel;
import com.app.infogain.androidcdp.ui.fragment.ContentListFragment;

import java.util.ArrayList;

/**
 * Created by rohit.mehta on 8/30/2016.
 */
public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {

    private ArrayList<NotificationModel> dataSet;
    Context context;

    public static class MyViewHolder extends RecyclerView.ViewHolder {


        TextView tvheadingNotifnum;
        TextView tvheadingNotif;
        TextView tvSubheadingNotif;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.tvheadingNotifnum = (TextView) itemView.findViewById(R.id.tvheadingNotifnum);
            this.tvheadingNotif = (TextView) itemView.findViewById(R.id.tvheadingNotif);
            this.tvSubheadingNotif = (TextView) itemView.findViewById(R.id.tvSubheadingNotif);

        }
    }

    public NotificationAdapter(ArrayList<NotificationModel> data, Context context) {
        this.dataSet = data;
        this.context = context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_layout, parent, false);

        // view.setOnClickListener(NotificationAdapter.this);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {


        TextView tvheadingNotifnum = holder.tvheadingNotifnum;
        TextView tvheadingNotif = holder.tvheadingNotif;
        TextView tvSubheadingNotif = holder.tvSubheadingNotif;
        String message = dataSet.get(listPosition).getHeading();
        tvheadingNotif.setText(message);
        //  tvheadingNotif.setText(dataSet.get(listPosition).getHeading());
        tvSubheadingNotif.setText(dataSet.get(listPosition).getSubheading());
        Typeface subheading_font = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
        Typeface heading_font = Typeface.createFromAsset(context.getAssets(), "fonts/Rafale_bg.otf");

        tvSubheadingNotif.setTypeface(subheading_font);
        tvheadingNotif.setTypeface(heading_font);
        int counter = listPosition + 1;
        tvheadingNotifnum.setTypeface(heading_font);
        if (listPosition < 9) {
            tvheadingNotifnum.setText("0" + counter);
        } else {
            tvheadingNotifnum.setText(counter + "");
        }
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }


}
