package com.app.infogain.androidcdp.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.app.infogain.androidcdp.R;
import com.app.infogain.androidcdp.model.TabsSubModule;
import com.app.infogain.androidcdp.ui.activities.WebViewContent;
import com.app.infogain.androidcdp.util.CheckNetwork;
import com.app.infogain.androidcdp.util.VolleyImageRequest;

/**
 * Created by saurabh.khanna on 8/30/2016.
 */
public class ContentDetailsAdapter extends RecyclerView.Adapter<ContentDetailsAdapter.MyViewHolder> {

  // private ArrayList<TabsSubmoduleItems> tabcontent;
    private TabsSubModule tabsSubModule;
    private Boolean ItIsVideoTabOrNot;
    private Activity activity;
    Typeface heading_font;
    Typeface description_font;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewTitle;
        TextView textViewDescription;
        ImageView imageViewVideo;
        ImageView imageOverLay;
        ImageView tv_base_image;


        public MyViewHolder(View itemView,int viewType) {
            super(itemView);
            if(viewType==0) {
                this.textViewTitle = (TextView) itemView.findViewById(R.id.tv_title);
                this.textViewDescription = (TextView) itemView.findViewById(R.id.tv_title_description);
                this.imageViewVideo = (ImageView) itemView.findViewById(R.id.iv_video);
                this.imageOverLay=(ImageView)itemView.findViewById(R.id.imageOverlay);
                this.tv_base_image = (ImageView) itemView.findViewById(R.id.iv_base_image);
            }else
            {
                this.textViewTitle = (TextView) itemView.findViewById(R.id.tv_title);
                this.textViewDescription = (TextView) itemView.findViewById(R.id.tv_title_description);
                this.tv_base_image = (ImageView) itemView.findViewById(R.id.iv_base_image);
            }
        }
    }

    public ContentDetailsAdapter(TabsSubModule subModule, Boolean ItIsVideoTabOrNot, Activity activity) {
      //  this.tabcontent = tabcontent;
        this.tabsSubModule= subModule;
        this.ItIsVideoTabOrNot = ItIsVideoTabOrNot;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.content_details_adapter, parent, false);
        View contentview = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sub_module_content_item, parent, false);
       /* mDefaultImage= BitmapFactory.decodeResource(activity.getResources(),
                R.drawable.);*/
        // view.setOnClickListener(CustomAdapter.this);
        MyViewHolder myViewHolder;
        if(viewType==1)
             myViewHolder = new MyViewHolder(contentview,1);
        else
             myViewHolder = new MyViewHolder(view,0);


        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        holder.textViewTitle.setText(tabsSubModule.getTabs().get(listPosition).getTitle());
        holder.textViewDescription.setText(tabsSubModule.getTabs().get(listPosition).getDescription());
        heading_font = Typeface.createFromAsset(activity.getAssets(),  "fonts/Rafale_bg.otf");
        description_font = Typeface.createFromAsset(activity.getAssets(),  "fonts/Roboto-Light.ttf");
        holder.textViewTitle.setTypeface(heading_font);
        holder.textViewDescription.setTypeface(description_font);

        if (getItemViewType(0)==0) {
            holder.imageViewVideo.setVisibility(View.VISIBLE);
            holder.tv_base_image.setVisibility(View.GONE);
            String[] imagename=tabsSubModule.getTabs().get(listPosition).getContent().split("=");
            String url="http://img.youtube.com/vi/"+imagename[1]+"/0.jpg";
            holder.textViewDescription.setVisibility(View.GONE);
            ImageLoader imageLoader = VolleyImageRequest.getInstance(activity)
                    .getImageLoader();
            imageLoader.get(url, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {

                    if (response.getBitmap() != null) {
                        holder.imageViewVideo.setImageBitmap(response.getBitmap());
                        holder.imageOverLay.setVisibility(View.VISIBLE);
                    } else {
                        holder.imageViewVideo.setImageResource(R.drawable.default_video);
                    }
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    holder.imageOverLay.setVisibility(View.GONE);
                    holder.imageViewVideo.setImageResource(R.drawable.default_video);

                }
            });
           /*
            imageLoader.get(url, ImageLoader.getImageListener(holder.imageViewVideo,
                    R.drawable.default_video, R.drawable
                            .default_video));
*/
        } else {
            if (getItemViewType(0)==0)
            holder.imageViewVideo.setVisibility(View.GONE);
            holder.tv_base_image.setVisibility(View.VISIBLE);
            holder.textViewDescription.setVisibility(View.VISIBLE);

        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CheckNetwork.checkConnection(activity))
                {
                    Intent webViewIntent = new Intent(activity, WebViewContent.class);
                    webViewIntent.putExtra("contentLink", tabsSubModule.getTabs().get(listPosition).getContent());
                    activity.startActivity(webViewIntent);
                }else{
                    Toast.makeText(activity,"please check internet connection",Toast.LENGTH_SHORT).show();
                }


            }
        });
    }


    @Override
    public int getItemViewType(int position) {
        if(tabsSubModule.getTab_title().equals("content") || tabsSubModule.getTab_title().equals("examples") )
            return 1;
        else if(tabsSubModule.getTab_title().equals("video"))
            return 0;
        else return 0;

    }

    @Override
    public int getItemCount() {
        return tabsSubModule.getTabs().size();
    }


}