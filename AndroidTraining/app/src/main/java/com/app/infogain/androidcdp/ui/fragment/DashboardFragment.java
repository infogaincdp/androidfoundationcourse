package com.app.infogain.androidcdp.ui.fragment;

import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.infogain.androidcdp.R;
import com.app.infogain.androidcdp.adapter.SpacesItemDecoration;
import com.app.infogain.androidcdp.adapter.StaggeredListAdapter;
import com.app.infogain.androidcdp.model.Module;
import com.app.infogain.androidcdp.ui.activities.ContentDetailActivity;
import com.app.infogain.androidcdp.util.AppUtils;
import com.app.infogain.androidcdp.util.MyApplication;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link DashboardFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DashboardFragment extends Fragment {

    private RecyclerView mRecyclerView;
    Module module = null;
    private FirebaseAnalytics mFirebaseAnalytics;
    private Tracker mTracker;

    public DashboardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
    }

    /**
     * Use this factory method to create a new instance of
     *
     * @return A new instance of fragment DashboardFragment.
     */
    public static DashboardFragment newInstance() {
        DashboardFragment fragment = new DashboardFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        AssetManager assetManager = getActivity().getAssets();

        MyApplication application = (MyApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();

        try {
            InputStream ims = assetManager.open("module.json");
            Reader reader = new InputStreamReader(ims);
            Gson gson = new Gson();
            module = gson.fromJson(reader, Module.class);

        } catch (IOException e) {
            e.printStackTrace();
        }


        mRecyclerView = (RecyclerView) view.findViewById(R.id.dashboard_grid);
        mRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));
        mRecyclerView.setClickable(true);

        StaggeredListAdapter adapter = new StaggeredListAdapter(getActivity(), module);
        mRecyclerView.setAdapter(adapter);
        SpacesItemDecoration decoration = new SpacesItemDecoration(2, getActivity());
        mRecyclerView.addItemDecoration(decoration);

        adapter.setOnItemClickListener(new StaggeredListAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {

                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("Action")
                        .setAction("Dashoard item clicked")
                        .setLabel("Item Name :"+module.getModuleItems().get(position).getStringmodule_title())
                        .build());

                AppUtils.LogAnalyticsEvent(mFirebaseAnalytics,""+position,module.getModuleItems().get(position).getStringmodule_title(), FirebaseAnalytics.Param.CONTENT_TYPE,"content",FirebaseAnalytics.Event.VIEW_ITEM_LIST);

                Intent tabIntent=new Intent(getActivity(), ContentDetailActivity.class);
                tabIntent.putExtra("ModulePos",module.getModuleItems().get(position).getModule_id());
                getActivity().startActivity(tabIntent);
            }

            @Override
            public void onItemLongClick(int position, View v) {

            }
        });

        return view;
    }
}
