package com.app.infogain.androidcdp.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sujeet1.kumar on 8/24/2016.
 */
public class ModuleItem {
    @SerializedName("module_id")
    String module_id;
    @SerializedName("module_title")
    String Stringmodule_title;
    @SerializedName("thumbnail")
    String thumbnail;
    @SerializedName("display_type")
    String display_type;

    public String getModule_id() {
        return module_id;
    }

    public void setModule_id(String module_id) {
        this.module_id = module_id;
    }

    public String getStringmodule_title() {
        return Stringmodule_title;
    }

    public void setStringmodule_title(String stringmodule_title) {
        Stringmodule_title = stringmodule_title;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDisplay_type() {
        return display_type;
    }

    public void setDisplay_type(String display_type) {
        this.display_type = display_type;
    }
}
