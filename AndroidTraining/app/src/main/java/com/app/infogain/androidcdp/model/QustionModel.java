package com.app.infogain.androidcdp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by sujeet1.kumar on 8/26/2016.
 */
public class QustionModel implements Parcelable {
    String question;
    String ans;
    String currentSelection;
    boolean isCorrect;
    boolean isAttamptQuestion ;
    ArrayList<OptionModel> option = new ArrayList<OptionModel>();

    public boolean isAttamptQuestion() {
        return isAttamptQuestion;
    }

    public void setAttamptQuestion(boolean attamptQuestion) {
        isAttamptQuestion = attamptQuestion;
    }




    public String getCurrentSelection() {
        return currentSelection;
    }

    public void setCurrentSelection(String currentSelection) {
        this.currentSelection = currentSelection;
    }

    public boolean isCorrect() {
        return isCorrect;
    }

    public void setCorrect(boolean correct) {
        isCorrect = correct;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAns() {
        return ans;
    }

    public void setAns(String ans) {
        this.ans = ans;
    }

    public ArrayList<OptionModel> getOption() {
        return option;
    }

    public void setOption(ArrayList<OptionModel> option) {
        this.option = option;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(question);
        parcel.writeString(ans);
       // parcel.writeList(option);
        parcel.writeString(currentSelection);
        parcel.writeByte((byte) (isCorrect ? 1 : 0));
        parcel.writeByte((byte)(isAttamptQuestion?1:0));

    }

    protected QustionModel(Parcel in) {
        question = in.readString();
        ans = in.readString();
       // option = in.readArrayList(null);
        currentSelection = in.readString();
        isCorrect = in.readByte() != 0;
        isAttamptQuestion = in.readByte()!=0 ;
    }

    public static final Creator<QustionModel> CREATOR = new Creator<QustionModel>() {
        @Override
        public QustionModel createFromParcel(Parcel in) {
            return new QustionModel(in);
        }

        @Override
        public QustionModel[] newArray(int size) {
            return new QustionModel[size];
        }
    };


}
