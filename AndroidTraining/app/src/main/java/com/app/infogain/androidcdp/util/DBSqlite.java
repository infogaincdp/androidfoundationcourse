package com.app.infogain.androidcdp.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Rohit.Mehta on 6/29/2016.
 */
public class DBSqlite extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Notifications.db";
    public static final String NOTIFICATIONS_TABLE_NAME = "Notifications";
    public static final String NOTIFICATIONS_COLUMN_ID = "id";
    public static final String NOTIFICATIONS_COLUMN_MSG = "message";
    public static final String NOTIFICATIONS_COLUMN_TITLE = "title";
    private HashMap hp;

    public DBSqlite(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(
                "create table " + NOTIFICATIONS_TABLE_NAME +
                        "(id integer primary key AUTOINCREMENT not null,title text, message text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS " + NOTIFICATIONS_TABLE_NAME);
        onCreate(db);
    }

    public boolean addNotification(String title,String message) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NOTIFICATIONS_COLUMN_MSG, message);
        contentValues.put(NOTIFICATIONS_COLUMN_TITLE, title);
        db.insert(NOTIFICATIONS_TABLE_NAME, null, contentValues);
        return true;
    }

    public Cursor getAllNotifications() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + NOTIFICATIONS_TABLE_NAME + " ORDER BY id DESC", null);
        return res;
    }

    public Boolean getNotification(String msg) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + NOTIFICATIONS_TABLE_NAME + " where message = '" + msg+"'", null);
        if (res != null)
            return true;
        else
            return false;
    }
}