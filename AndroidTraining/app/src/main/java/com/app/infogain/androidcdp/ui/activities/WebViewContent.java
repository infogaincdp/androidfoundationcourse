package com.app.infogain.androidcdp.ui.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.app.infogain.androidcdp.R;
import com.app.infogain.androidcdp.util.AppUtils;
import com.app.infogain.androidcdp.util.MyApplication;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;

public class WebViewContent extends AppCompatActivity {
    WebView webView;
    Bundle extras;
    String contentUrl;
    ProgressDialog progressdialog;
    private FirebaseAnalytics mFirebaseAnalytics;
    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view_content);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        MyApplication application = (MyApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("WebView Screen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        AppUtils.LogAnalyticsEvent(mFirebaseAnalytics,"","web view",FirebaseAnalytics.Param.CONTENT_TYPE,"content","browser open");

        webView = (WebView) findViewById(R.id.webview);
        extras = getIntent().getExtras();
        contentUrl = extras.getString("contentLink");
        webView.getSettings().setJavaScriptEnabled(true);
        progressdialog = ProgressDialog.show(this, "", "please wait...");
        progressdialog.setCancelable(false);
        progressdialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                webView.stopLoading();
            }
        });
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                progressdialog.setProgress(progress * 100);
            }
        });
        webView.setWebViewClient(new DetailWebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.loadUrl(contentUrl);
    }

    public class DetailWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            progressdialog.setCancelable(true);
            return true;
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            Toast.makeText(WebViewContent.this, "Oh no! " + description, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (progressdialog != null && progressdialog.isShowing()) {
                progressdialog.dismiss();
            }
        }
    }
}
