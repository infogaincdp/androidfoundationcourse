package com.app.infogain.androidcdp.listner;

import com.app.infogain.androidcdp.model.QustionModel;

/**
 * Created by sujeet1.kumar on 8/29/2016.
 */
public interface SelectAnswerListner {

    public void answerCallBack(QustionModel qustionModel ,int position);
}
