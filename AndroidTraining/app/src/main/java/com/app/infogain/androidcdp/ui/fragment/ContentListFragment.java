package com.app.infogain.androidcdp.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.infogain.androidcdp.R;
import com.app.infogain.androidcdp.adapter.CustomAdapter;
import com.app.infogain.androidcdp.listner.DashboardClickListner;
import com.app.infogain.androidcdp.model.DataModel;
import com.app.infogain.androidcdp.ui.activities.DashboardActivity;
import com.app.infogain.androidcdp.ui.activities.TestActivity;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link ContentListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ContentListFragment extends Fragment implements DashboardClickListner {
    static String[] nameArray = {"Training Content", "MCQ"};

    public static Integer[] drawableArray = {R.drawable.tcontectimage, R.drawable.tcontectimage2};


    static Integer[] id_ = {0, 1};


    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<DataModel> data;

    public ContentListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * @return A new instance of fragment ContentListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ContentListFragment newInstance() {
        ContentListFragment fragment = new ContentListFragment();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_content_list, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.content_list);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        data = new ArrayList<DataModel>();
        for (int i = 0; i < nameArray.length; i++) {
            data.add(new DataModel(
                    nameArray[i],
                    id_[i],
                    drawableArray[i]
            ));
        }

        adapter = new CustomAdapter(data,ContentListFragment.this,getActivity());
        recyclerView.setAdapter(adapter);
        return  view ;
    }

    @Override
    public void itemOnClick(int pos) {
        if (pos!=1)
             getActivity().startActivity(new Intent(getActivity(), DashboardActivity.class));
        else
            getActivity().startActivity(new Intent(getActivity(), TestActivity.class));
    }


}
