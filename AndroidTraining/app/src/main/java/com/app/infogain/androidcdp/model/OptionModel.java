package com.app.infogain.androidcdp.model;

/**
 * Created by sujeet1.kumar on 8/26/2016.
 */
public class OptionModel {
    String option_id ;
    String option_name;

    public String getOption_name() {
        return option_name;
    }

    public void setOption_name(String option_name) {
        this.option_name = option_name;
    }

    public String getOption_id() {
        return option_id;
    }

    public void setOption_id(String option_id) {
        this.option_id = option_id;
    }
}
