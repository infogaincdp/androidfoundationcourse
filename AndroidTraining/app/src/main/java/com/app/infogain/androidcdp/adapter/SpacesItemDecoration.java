package com.app.infogain.androidcdp.adapter;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.app.infogain.androidcdp.util.AppUtils;

/**
 * Created by sarvaraj on 24/08/16.
 */
public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private final int mSpaceMargin;
    Context mContext;

    public SpacesItemDecoration(int space, Context mContext) {
        this.mSpaceMargin = space;
        this.mContext = mContext;

    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

        outRect.right = mSpaceMargin;
        outRect.bottom = mSpaceMargin;


        if (parent.getChildAdapterPosition(view) == 9) {
            ViewGroup.LayoutParams param = view.getLayoutParams();
            param.width = AppUtils.getDeviceWidth(mContext);
            view.setLayoutParams(param);
        } else {
        }
    }
}