package com.app.infogain.androidcdp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by saurabh.khanna on 8/29/2016.
 */
public class TabsSubModule implements Parcelable {

    @SerializedName("tab_id")
    String tab_id;
    @SerializedName("tab_title")
    String tab_title;

    @SerializedName("sub_module")
    ArrayList<TabsSubmoduleItems> tabs;


    public String getTab_id() {
        return tab_id;
    }

    public void setTab_id(String module_id) {
        this.tab_id = module_id;
    }

    public String getTab_title() {
        return tab_title;
    }

    public void setTab_title(String tab_title) {
        this.tab_title = tab_title;
    }

    public ArrayList<TabsSubmoduleItems> getTabs() {
        return tabs;
    }

    public void setTabs(ArrayList<TabsSubmoduleItems> tabs) {
        this.tabs = tabs;
    }


    public static final Creator<TabsSubModule> CREATOR = new Creator<TabsSubModule>() {
        @Override
        public TabsSubModule createFromParcel(Parcel in) {
            return new TabsSubModule(in);
        }

        @Override
        public TabsSubModule[] newArray(int size) {
            return new TabsSubModule[size];
        }
    };

    protected TabsSubModule(Parcel in) {
        tab_id = in.readString();
        tab_title = in.readString();
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(tab_id);
        parcel.writeString(tab_title);
    }
}
