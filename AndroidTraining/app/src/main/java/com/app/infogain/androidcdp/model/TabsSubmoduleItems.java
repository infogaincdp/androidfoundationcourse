package com.app.infogain.androidcdp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by saurabh.khanna on 8/29/2016.
 */
public class TabsSubmoduleItems implements Parcelable {



    @SerializedName("title")
    String title;
    @SerializedName("description")
    String description;

    @SerializedName("content")
    String content;

    protected TabsSubmoduleItems(Parcel in) {
        title = in.readString();
        description = in.readString();
        content = in.readString();
    }

    public static final Creator<TabsSubmoduleItems> CREATOR = new Creator<TabsSubmoduleItems>() {
        @Override
        public TabsSubmoduleItems createFromParcel(Parcel in) {
            return new TabsSubmoduleItems(in);
        }

        @Override
        public TabsSubmoduleItems[] newArray(int size) {
            return new TabsSubmoduleItems[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getContent() {
        return content;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setContent(String content) {
        this.content = content;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeString(content);
    }
}
