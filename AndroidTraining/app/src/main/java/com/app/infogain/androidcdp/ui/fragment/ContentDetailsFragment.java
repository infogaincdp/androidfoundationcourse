package com.app.infogain.androidcdp.ui.fragment;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.infogain.androidcdp.R;
import com.app.infogain.androidcdp.adapter.ContentDetailsAdapter;
import com.app.infogain.androidcdp.model.TabsSubModule;
import com.app.infogain.androidcdp.model.TabsSubmoduleItems;

import java.util.ArrayList;

/**
 * Created by sarvaraj on 26/08/16.
 * <p>
 * edited by Saurabh.khanna on 30/08/16.
 */
public class ContentDetailsFragment extends Fragment {

    private Bundle bundle;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    ContentDetailsAdapter contentDetailsAdapter;
    ArrayList<TabsSubmoduleItems> arrayList;
    TabsSubModule subModule;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.content_details_fragment, container, false);
        bundle = getArguments();
        recyclerView = (RecyclerView) view.findViewById(R.id.content_details_rv);

        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        arrayList = new ArrayList<TabsSubmoduleItems>();
        subModule = bundle.getParcelable("tabsData");
        contentDetailsAdapter = new ContentDetailsAdapter(subModule,bundle.getBoolean("ItIsVideoTab"),getActivity());
        recyclerView.setAdapter(contentDetailsAdapter);
        return view;
    }

}
