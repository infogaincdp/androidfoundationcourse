package com.app.infogain.androidcdp.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.infogain.androidcdp.R;
import com.app.infogain.androidcdp.constant.AppConstant;
import com.app.infogain.androidcdp.model.QustionModel;

import java.util.ArrayList;

/**
 * Created by sujeet1.kumar on 8/30/2016.
 */


public class TestResultFragment extends Fragment {

    ArrayList<QustionModel> qustionLists = new ArrayList<>();
    int correctCount=0;
    int inCorrectCount=0;
    int skippedCount=0;
    public TestResultFragment() {
        // Required empty public constructor
    }
    public static TestResultFragment newInstant() {
        return new TestResultFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_test_result,container,false);

        qustionLists =  getArguments().getParcelableArrayList(AppConstant.TEST_RESULT_LIST);

        TextView txt_marks_obtain = (TextView)view.findViewById(R.id.obtain_marks);
        TextView txt_total_marks = (TextView)view.findViewById(R.id.total_marks);


        TextView txt_correct = (TextView)view.findViewById(R.id.correct_ans);
        TextView txt_inCorrect = (TextView)view.findViewById(R.id.incorrect_ans);
        TextView txt_skipped = (TextView)view.findViewById(R.id.skipped_ans);

        txt_marks_obtain.setText(""+getCorrectanswer()*2);
        txt_total_marks.setText(""+qustionLists.size()*2);
        txt_correct.setText(""+getCorrectanswer()+" CORRECT");
        txt_inCorrect.setText(""+getInCorrectanswer()+" INCORRECT");
        txt_skipped.setText(""+getSkippedanswer()+" SKIPPED");



        return view ;
    }

    int getCorrectanswer()
    {
        correctCount=0;
        for(int i=0;i<qustionLists.size();i++)
        {
            if(qustionLists.get(i).isCorrect())
                correctCount=correctCount+1;
        }
        return correctCount;
    }

    int getInCorrectanswer()
    {
        inCorrectCount=0;
        for(int i=0;i<qustionLists.size();i++)
        {
            if(!qustionLists.get(i).isCorrect() && qustionLists.get(i).isAttamptQuestion())
                inCorrectCount=inCorrectCount+1;
        }
        return inCorrectCount;
    }
    int getSkippedanswer()
    {
        skippedCount =0;
        for(int i=0;i<qustionLists.size();i++)
        {
            if(qustionLists.get(i).isAttamptQuestion())
                skippedCount=skippedCount+1;
        }
        return qustionLists.size()-skippedCount;
    }
}
