package com.app.infogain.androidcdp.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.toolbox.StringRequest;
import com.app.infogain.androidcdp.R;
import com.app.infogain.androidcdp.util.DBSqlite;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by   on 22/08/16.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.e(TAG, "From: " + remoteMessage.getFrom());
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            // From: 194431851071
            remoteMessage.getNotification().getBody();
            Log.e(TAG, "Message data payload: " + remoteMessage.getData());
            DBSqlite dbSqlite = new DBSqlite(getBaseContext());
            String message = remoteMessage.getData().toString().split("=")[1];
            message = message.substring(0, message.lastIndexOf(","));
            String title = remoteMessage.getData().toString().split("=")[2];
            title = title.substring(0, title.lastIndexOf("}"));
            dbSqlite.addNotification(title,message);
            sendNotification(title,message);
           /* if (dbSqlite.addNotification(title, message)) {
                sendNotification(title, message);
            }*/
        }

        // Check if message contains a notification payload.
      /*  if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            DBSqlite dbSqlite = new DBSqlite(getBaseContext());
            String message = remoteMessage.getData().toString().split("=")[1];
            message = message.substring(0, message.length() - 1);
            if (dbSqlite.addNotification(message)) {
                sendNotification(message);
            }

        }*/
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }


    /**
     * Create and show a simple notification containing the received FCM message.
     */
    public void sendNotification(String title, String message) {
        //  String messageBody=remoteMessage.ge;
        Intent intent = new Intent(this, com.app.infogain.androidcdp.ui.activities.NotificationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);
        int num = 0;
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.notification_icons)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setNumber(num)
                .setGroup("CDP_NOTIF_GRP")
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}