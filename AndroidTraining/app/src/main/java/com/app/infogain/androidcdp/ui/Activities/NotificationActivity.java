package com.app.infogain.androidcdp.ui.activities;

import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.app.infogain.androidcdp.R;
import com.app.infogain.androidcdp.adapter.NotificationAdapter;
import com.app.infogain.androidcdp.model.NotificationModel;
import com.app.infogain.androidcdp.util.AppUtils;
import com.app.infogain.androidcdp.util.DBSqlite;
import com.app.infogain.androidcdp.util.MyApplication;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;

public class NotificationActivity extends MainActivity {
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    ArrayList<NotificationModel> data;
    NotificationAdapter adapter;
    DBSqlite dbSqlite;
    Toolbar toolbar;
    Typeface heading_font;
    TextView mTitle;
    TextView mTitletextViewnonotif;
    private FirebaseAnalytics mFirebaseAnalytics;
    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        AppUtils.LogAnalyticsEvent(mFirebaseAnalytics,"","notification Screen",FirebaseAnalytics.Param.CONTENT_TYPE,"notification", FirebaseAnalytics.Event.VIEW_ITEM_LIST);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        MyApplication application = (MyApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Notification Screen")
                .build());
        AppUtils.LogAnalyticsEvent(mFirebaseAnalytics,"","notification Screen",FirebaseAnalytics.Param.CONTENT_TYPE,"notification",FirebaseAnalytics.Event.VIEW_ITEM_LIST);

        toolbar = (Toolbar) findViewById(R.id.notif_toolbar);
        toolbar.setTitle("");
        toolbar.setTitleTextColor(getResources().getColor(R.color.color_white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        heading_font = Typeface.createFromAsset(getResources().getAssets(), "fonts/Rafale_bg.otf");
        mTitle = (TextView) findViewById(R.id.TVToolbarTitleNotif);
        mTitle.setText("Notifications");
        mTitle.setTypeface(heading_font);
        recyclerView = (RecyclerView) findViewById(R.id.rvNotification);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        data = new ArrayList<NotificationModel>();

        dbSqlite = new DBSqlite(getApplicationContext());
        Cursor res = dbSqlite.getAllNotifications();
        mTitletextViewnonotif = (TextView) findViewById(R.id.textViewnonotif);
        mTitletextViewnonotif.setTypeface(heading_font);
        if (res.isAfterLast()) {
            recyclerView.setVisibility(View.GONE);
            mTitletextViewnonotif.setVisibility(View.VISIBLE);
            Log.d("No notifications", "No notifications");

        } else {
            res.moveToFirst();
            recyclerView.setVisibility(View.VISIBLE);
            mTitletextViewnonotif.setVisibility(View.GONE);
            while (!res.isAfterLast()) {
                data.add(new NotificationModel(res.getString(res.getColumnIndex("title")), res.getString(res.getColumnIndex("message"))));
                res.moveToNext();
            }
        }

        adapter = new NotificationAdapter(data, this);
        recyclerView.setAdapter(adapter);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onBackPressed();
        return super.onOptionsItemSelected(item);

    }

}
