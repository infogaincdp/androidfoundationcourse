package com.app.infogain.androidcdp.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.app.infogain.androidcdp.R;
import com.app.infogain.androidcdp.util.AppUtils;

import com.app.infogain.androidcdp.util.DBSqlite;
import com.app.infogain.androidcdp.util.MyApplication;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;

public class SplashActivity extends AppCompatActivity {

    private FirebaseAnalytics mFirebaseAnalytics;
    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        DBSqlite dbSqlite = new DBSqlite(this);
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().get("data")!=null &&getIntent().getExtras().get("title")!=null )
            {
                try{
                    String msg = getIntent().getExtras().get("data").toString();
                    String title = getIntent().getExtras().get("title").toString();
                    if (dbSqlite.getNotification(msg)) {
                        dbSqlite.addNotification(title, msg);
                        startActivity(new Intent(SplashActivity.this, NotificationActivity.class));
                        finish();
                    }
                }catch (Exception e)
                {
                    e.printStackTrace();
                }

            }else{
                callNextActivity();
            }
        }else {
            callNextActivity();
        }
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        MyApplication application = (MyApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Splash Screen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        AppUtils.LogAnalyticsEvent(mFirebaseAnalytics, "", "Splash Screen", FirebaseAnalytics.Param.CONTENT_TYPE, "image", FirebaseAnalytics.Event.APP_OPEN);
    }

    private void callNextActivity() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, ContentListActivity.class));
                finish();
            }
        }, 1000);
    }


}
