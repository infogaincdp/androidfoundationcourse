package com.app.infogain.androidcdp.listner;

/**
 * Created by sujeet1.kumar on 8/26/2016.
 */
public interface DashboardClickListner {
    void itemOnClick(int pos);
}
