package com.app.infogain.androidcdp.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 *
 * Created by naina on 20/11/15.
 */
public class CheckNetwork {
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    public static Boolean checkConnection(Context context)
    {
        try
        {
            ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mobileNetworkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            NetworkInfo wifiNetworkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            if(wifiNetworkInfo.isConnected() || mobileNetworkInfo.isConnected())
            {
                return true;
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return false;
    }

}
