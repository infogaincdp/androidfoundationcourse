package com.app.infogain.androidcdp.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by sarvaraj on 26/08/16.
 */
public class SubModuleContent {
    @SerializedName("module_id")
    String moduleId;
    @SerializedName("tabs")
    ArrayList<Tabs> tabs = new ArrayList<Tabs>();

    ArrayList<Tabs> getTabs()
    {
        return this.tabs;
    }
}
